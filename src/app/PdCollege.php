<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class PdCollege
 * @property College college
 * @package App
 */
class PdCollege extends Model
{

    protected $fillable = [
        'college'
    ];

    public function __construct(College $college)
    {
        parent::__construct(['college' => $college]);
    }

    public static function colleges()
    {
        return College::query()
            ->orderBy('name', 'asc');
    }

    public function departments()
    {
        if ( $this->college instanceof College ) {
            return $this->college->departmentsWithPdRoster();
        } else {
            dd('Should query Departments for Unknown College');
        }
    }

//    public static function departments(College $college)
//    {
//        $collegeId = $college->id;
//        return Department::query()
//            ->select([
//                'departments.*'
//            ])
//            ->join('wku_identities as wi', function ($join) use ($collegeId) {
//                $join->on('wi.department_id', '=', 'departments.id');
//                $join->where('wi.college_id', '=', $collegeId);
//            })
//            ->orderBy('departments.name', 'asc')
//            ->groupBy('departments.id');
//    }

    public static function departmentsWithRoster(College $college, string $pdrAttendedValue = NULL)
    {
        $pdCollege = new PdCollege($college);
        $departments = $pdCollege->departments()
            ->select([
                'departments.*',
            ])
            ->join('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.wku_identity_id', '=', 'wi.id');
            });

        if ($pdrAttendedValue !== NULL) {
            $departments->where('attended', '=', $pdrAttendedValue);
        }

        return $departments;
    }

    public function departmentsWithPdRosters(string $professionalDevelopmentRosterAttendedValue = NULL)
    {
        if ( $this->college instanceof College ) {
            $departmentIdsWithRoster = $this->college->professionalDevelopmentRosters($professionalDevelopmentRosterAttendedValue)
                ->select([
//                'professional_development_rosters.*',
                    'wi.department_id as wku_identity_department_id'
                ])
                ->groupBy('wku_identity_department_id')
                ->pluck('wku_identity_department_id');
//        ;
//        $departmentsWithPdRosters = [];
            dd($departmentIdsWithRoster);
        } else {
            dd('need to Query where Department is not set');
        }
    }

    public static function departmentsWithRosterAttendedCount(College $college)
    {
        $pdCollege = new static($college);
        return $pdCollege->departments()
            ->select([
                'departments.*',
                DB::raw('count(pdr.id) as roster_attended_count')
            ])
            ->join('professional_development_rosters as pdr', function ($join) {
//                $join->on('pdr.wku_identity_id', '=', 'wi.id');
                $join->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
            });
    }

    public static function departmentsWithRosterAttendedUniqueCount(College $college)
    {
        $professionalDevelopmentRosterWkuIdentitiesUnique = $college->professionalDevelopmentRostersWkuIdentityUnique();
//        dd($professionalDevelopmentRostersUnique->get());

        $departmentCounts = [];
        foreach ($professionalDevelopmentRosterWkuIdentitiesUnique->get() as $professionalDevelopmentRosterWkuIdentityUnique) {
//            dd($professionalDevelopmentRosterWkuIdentityUnique);
            $departmentId = $professionalDevelopmentRosterWkuIdentityUnique->department_id;
            if ($departmentId == NULL) {
                $departmentId = 'Unknown';
            }
            if (!array_key_exists($departmentId, $departmentCounts)) {
                $departmentCounts[$departmentId] = 0;
            }
            $departmentCounts[$departmentId]++;
        }

//        dd($departmentCounts);

        $pdCollege = new static($college);
        $departments = $pdCollege->departments()->get();

        foreach ($departments as $department) {
            $departmentId = $department->id;
            if (array_key_exists($departmentId, $departmentCounts)) {
                $department->pd_roster_attented_wku_identity_count = $departmentCounts[$departmentId];
            }
        }

//        dd($departments);

        return $departments;
    }

}
