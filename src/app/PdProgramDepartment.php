<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PdProgramDepartment
 * @property Department department
 * @property ProfessionalDevelopmentProgram professionalDevelopmentProgram
 * @package App
 */
class PdProgramDepartment extends Model
{

    public function attendanceForDepartment(Department $department, string $attendedValue = NULL)
    {
        $professionalDevelopmentProgramId = $this->professionalDevelopmentProgram->id;

        return (new ProfessionalDevelopmentRoster())
            ->attendanceForDepartment($department, $attendedValue)
            ->join('professional_development_sessions as pds', function($join) use($professionalDevelopmentProgramId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.professional_development_program_id', '=', $professionalDevelopmentProgramId);
            });
    }

}
