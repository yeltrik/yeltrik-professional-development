<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PdSemester extends Model
{

    protected $fillable = [
        'semester'
    ];

    public function __construct(Semester $semester)
    {
        parent::__construct(['semester' => $semester]);
    }

    public static function semesters()
    {
        return Semester::query()
            ->select([
                'semesters.*',
                DB::raw('count(pds.id) as professional_development_session_count')
            ])
            ->join('professional_development_sessions as pds', function($join){
                $join->on('pds.semester_id', '=', 'semesters.id');
            })
            ->groupBy('semesters.id');
    }

}
