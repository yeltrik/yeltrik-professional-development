<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PdSemesterCollege extends Model
{

    protected $fillable = [
        'semester',
        'college',
    ];

    public function __construct(Semester $semester, College $college)
    {
        parent::__construct([
            'semester' => $semester,
            'college' => $college,
        ]);
    }

    public function colleges()
    {
        $semesterId = $this->semester->id;
        return College::query()
            ->select(['colleges.*'])
            ->join('wku_identities as wi', function ($join) use ($semesterId) {
                $join->on('wi.college_id', '=', 'colleges.id');
                $join->join('professional_development_rosters as pdr', function ($join) use ($semesterId) {
                    $join->on('pdr.wku_identity_id', '=', 'wi.id');
                    $join->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                        $join->on('pds.id', '=', 'pdr.professional_development_session_id');
                        $join->where('pds.semester_id', '=', $semesterId);
                    });
                });
            })
            ->orderBy('colleges.name', 'asc')
            ->groupBy('colleges.id');
    }

    public function departments()
    {
        $pdCollege = new PdCollege($this->college);
        return $pdCollege->departments();
    }

    public function departmentsWithRosterAttendedCount()
    {
        $semesterId = $this->semester->id;
        return PdCollege::departmentsWithRosterAttendedCount($this->college)
            ->join('professional_development_sessions as pds', function($join) use($semesterId){
                $join->on('pds.id', '=', 'pdr.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            });
    }

}
