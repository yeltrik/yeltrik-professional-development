<?php

namespace App;

use App\Http\Controllers\PdRosterSignatureAsanaFacultyNameController;
use App\Http\Controllers\PdRosterSignatureRegistrationAttendanceController;
use Asana\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;

class PdSessionAsanaTaskController extends Model
{

    private array $signatureWkuRoster = [
        0 => "Attended",
        1 => "No Show",
        2 => "Cancelled",
        3 => "Email",
        4 => "WKUID",
        5 => "Enrolled",
        6 => "Waiting List",
        7 => "Actions",
    ];

    private array $signatureAttendedWkuIdName = [
        0 => 'Attended',
        1 => 'WKU ID', // Also Saw one without a Space
        2 => 'Name',
    ];

    private function csv($rows)
    {
        $array = array_map('str_getcsv', $rows);
        array_walk($array, function (&$a) use ($array) {
            if (sizeof($array[0]) === sizeof($a)) {
                $a = array_combine($array[0], $a);
            }
        });
        array_shift($array); # remove column header
        return $array;
    }

    private function getPdRoster(ProfessionalDevelopmentSession $professionalDevelopmentSession, User $user = NULL, WkuIdentity $wkuIdentity, string $attended)
    {
        $pdRoster = ProfessionalDevelopmentRoster::query()
            ->where('wku_identity_id', '=', $wkuIdentity->id)
            ->where('professional_development_session_id', '=', $professionalDevelopmentSession->id)
            ->first();
        if ($pdRoster === NULL) {
            $pdRoster = $this->newProfessionalDevelopmentRoster($professionalDevelopmentSession, $user, $wkuIdentity, $attended, ProfessionalDevelopmentRoster::STUDENT_ROLE);
        }
        return $pdRoster;
    }

    private function getUser(string $name, string $email)
    {
        $user = User::query()
            ->where('email', '=', $email)
            ->first();
        if ($user instanceof User === FALSE) {
            $user = $this->newUser($name, $email);
        }
        return $user;
    }

    private function getWkuIdentity(string $email = NULL, string $wkuId = NULL, string $name = NULL)
    {
        $query = WkuIdentity::query();

        if ($email !== NULL) {
            $query->orwhere('email', '=', $email);
        }
        if ($wkuId !== NULL) {
            $query->orWhere('wkuid', '=', $wkuId);
        }

        $wkuIdentity = $query->first();
        if ($wkuIdentity instanceof WkuIdentity === FALSE) {
            $wkuIdentity = $this->newWkuIdentity($email, $wkuId, $name);
        }
        return $wkuIdentity;
    }

    public function scan(ProfessionalDevelopmentSession $professionalDevelopmentSession)
    {
        $asana_client = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
        $asanaTask = $asana_client->tasks->getTask($professionalDevelopmentSession->asana_gid, [], [
            'opt_fields' => 'name, attachments, attachments.name, attachments.created_at, attachments.download_url, attachments.view_url'
        ]);

        $asanaTaskAttachments = $asanaTask->attachments;

        return view('pd.session.asana.task.attachments', compact('professionalDevelopmentSession', 'asanaTaskAttachments'));
    }

    public function asanaTaskAttachment(ProfessionalDevelopmentSession $professionalDevelopmentSession, $asanaTaskAttachmentGid)
    {
        $asana_client = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
        $attachment = $asana_client->attachments->getAttachment($asanaTaskAttachmentGid);

        $data = file_get_contents($attachment->view_url);
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (strncmp($data, $bom, 3) === 0) {
            $data = substr($data, 3);
        }
        $rows = explode("\n", $data);
        $array = $this->csv($rows);

        // Determine the Signature of this File to determine which import to process.
        if (sizeof($array) > 0) {
            $signature = array_keys($array[0]);
            asort($signature);

            asort($this->signatureWkuRoster);
            asort($this->signatureAttendedWkuIdName);

            $pdRosterSignatureRegistrationAttendanceController = new PdRosterSignatureRegistrationAttendanceController();
            $pdRosterSignatureAsanaFacultyNameController = new PdRosterSignatureAsanaFacultyNameController();

            if (empty(array_diff($signature, $this->signatureWkuRoster))) {
                return $this->processAsWkuRoster($professionalDevelopmentSession, $array);
            } elseif($pdRosterSignatureRegistrationAttendanceController->rosterMatches($array)) {
                $pdRosterSignatureRegistrationAttendanceController->process($professionalDevelopmentSession, $array);
            } elseif($pdRosterSignatureAsanaFacultyNameController->rosterMatches($array)) {
                $pdRosterSignatureAsanaFacultyNameController->process($professionalDevelopmentSession, $array);
            } elseif (empty(array_diff($signature, $this->signatureAttendedWkuIdName))) {
                return $this->processAsAttendedWkuIdName($professionalDevelopmentSession, $array);
            } else {
                echo '<h2>Unknown Signature of <u><em>' . $attachment->name . '</em></u> </h2>';
                if( $professionalDevelopmentSession->tags()->count() > 0 ) {
                    echo '<h3>Tags</h3>';
                }
                foreach ( $professionalDevelopmentSession->tags as $tag) {
                    echo '<div style="background-color: '. str_replace(['dark-', 'light-'], '', $tag->color) . ';">' . $tag->name . '</div>';
                }
                echo '<h3>Structure Provided</h3>';
                echo "<pre>" . var_export($signature, true) . "</pre>";
                echo "<hr>";
                echo '<h3>Supported Signatures</h3>';
                echo "WKU Roster:";
                echo "<pre>" . var_export($this->signatureWkuRoster, true) . "</pre>";
                echo "<hr>";
                echo "Registration Attendance:";
                echo "<pre>" . var_export($pdRosterSignatureRegistrationAttendanceController->getSignature(), true) . "</pre>";
                echo "<hr>";
                echo "Asana Faculty Name:";
                echo "<pre>" . var_export($pdRosterSignatureAsanaFacultyNameController->getSignature(), true) . "</pre>";
                echo "<hr>";
                echo "Attended WkuId Name:";
                echo "<pre>" . var_export($this->signatureAttendedWkuIdName, true) . "</pre>";
                echo "<hr>";
                dd($array);
            }
        }

        return null;
    }

    /**
     * @param ProfessionalDevelopmentSession $professionalDevelopmentSession
     * @param array $array
     * @return RedirectResponse
     */
    private function processAsAttendedWkuIdName(ProfessionalDevelopmentSession $professionalDevelopmentSession, array $array)
    {
        foreach ($array as $value) {
            $signature = array_keys($value);
            if (empty(array_diff($signature, $this->signatureAttendedWkuIdName))) {
                $attended = $value['Attended'];
                $wkuId = $value['WKU ID'];
                $name = $value['Name'];

                if ( $attended === 'Attended' ) {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_YES;
                } else {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_NO;
                }

                $wkuIdentity = $this->getWkuIdentity(NULL, $wkuId, $name);

                $this->getPdRoster($professionalDevelopmentSession, NULL, $wkuIdentity, $attendedValue);
            }
        }

        return redirect()->route('pds.sessions.show', $professionalDevelopmentSession);
    }

    /**
     * @param ProfessionalDevelopmentSession $professionalDevelopmentSession
     * @param array $array
     * @return RedirectResponse
     */
    private function processAsWkuRoster(ProfessionalDevelopmentSession $professionalDevelopmentSession, array $array)
    {
        foreach ($array as $value) {
            if (
                array_key_exists('Email', $value) &&
                array_key_exists('WKUID', $value) &&
                array_key_exists('Attended', $value)
            ) {
                $attended = $value['Attended'];
                $email = $value['Email'];
                $wkuId = $value['WKUID'];

                $user = $this->getUser($email, $email);
                // TODO: Could try to Put Department in WKU Identity
                $wkuIdentity = $this->getWkuIdentity($email, $wkuId);

                if ($user instanceof User) {
                    $wkuIdentity->user()->associate($user);
                    $wkuIdentity->save();
                }

                if ($attended === "Has Not Attended") {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_NO;
                } elseif ($attended === "Attended") {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_YES;
                } else {
                    dd("Unsupported Attended Value: " . $attended);
                }

                $this->getPdRoster($professionalDevelopmentSession, $user, $wkuIdentity, $attendedValue);
            }
        }

        return redirect()->route('pds.sessions.show', $professionalDevelopmentSession);
    }

    private function newProfessionalDevelopmentRoster(ProfessionalDevelopmentSession $professionalDevelopmentSession, User $user = NULL, WkuIdentity $wkuIdentity, string $attended, string $role)
    {
        $pdRoster = new ProfessionalDevelopmentRoster();
        $pdRoster->professionalDevelopmentSession()->associate($professionalDevelopmentSession);
        if ( $user instanceof User) {
            $pdRoster->user()->associate($user);
        }
        $pdRoster->wkuIdentity()->associate($wkuIdentity);
        $pdRoster->attended = $attended;
        $pdRoster->role = $role;
        $pdRoster->save();
        return $pdRoster;
    }

    private function newUser(string $name, string $email)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = Hash::make('NEED_TO_HAVE_DEFAULT_PASSWORD_FOR_IMPORT_STORED_IN_A_CENTRAL_PLACE');
        $user->save();
        return $user;
    }

    private function newWkuIdentity(string $email = NULL, string $wkuId = NULL, string $name = NULL)
    {
        $wkuIdentity = new WkuIdentity();
        if ($email !== NULL) {
            $wkuIdentity->email = $email;
        }
        if ($wkuId !== NULL) {
            $wkuIdentity->wkuid = $wkuId;
        }
        if ($name !== NULL) {
            $wkuIdentity->name = $name;
        }
        $wkuIdentity->save();
        return $wkuIdentity;
    }

}
