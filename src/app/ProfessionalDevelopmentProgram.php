<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ProfessionalDevelopmentProgram
 * @property int id
 * @property string title
 * @package App
 */
class ProfessionalDevelopmentProgram extends Model
{

    public function professionalDevelopmentSessions()
    {
        return $this->hasMany(ProfessionalDevelopmentSession::class);
    }

    public function programsForDepartmentWithAttendance(Department $department, string $attendanceValue)
    {
        $departmentId = $department->id;

        return ProfessionalDevelopmentProgram::query()
            ->select([
                'professional_development_programs.*',
                DB::raw("count(wi.id) as rosterCountYes"),
            ])
            ->join('professional_development_sessions as pds', function($join) use($departmentId, $attendanceValue) {
                $join->on('pds.professional_development_program_id', '=', 'professional_development_programs.id');
                $join->join('professional_development_rosters as pdr', function($join) use($departmentId, $attendanceValue) {
                    $join->on('pdr.professional_development_session_id', '=', 'pds.id');
                    $join->where('pdr.attended', '=', $attendanceValue);
                    $join->join('wku_identities as wi', function($join) use($departmentId) {
                        $join->on('wi.id', '=', 'pdr.wku_identity_id');
                        $join->where('wi.department_id', '=', $departmentId);
                    });
                });
            })
            ->orderBy('professional_development_programs.title', 'asc')
            ->orderBy('pds.title', 'asc')
            ->groupBy(['professional_development_programs.id']);
    }

}
