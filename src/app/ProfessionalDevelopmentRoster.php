<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfessionalDevelopmentRoster
 * @property int id
 * @property int professional_development_session_id
 * @property int user_id
 * @property int wku_identity_id
 * @property string role
 * @property string attended
 *
 * @property string title
 *
 * @property ProfessionalDevelopmentSession professional_development_session
 * @property User user
 * @property WkuIdentity wkuIdentity
 * @package App
 */
class ProfessionalDevelopmentRoster extends Model
{


    CONST STUDENT_ROLE = 'student';
    CONST TEACHER_ROLE = 'teacher';
    public static array $roles = [
        ProfessionalDevelopmentRoster::STUDENT_ROLE,
        ProfessionalDevelopmentRoster::TEACHER_ROLE,
    ];

    CONST ATTENDED_YES = 'Yes';
    CONST ATTENDED_NO = 'No';
    public static array $attendedOptions = [
        ProfessionalDevelopmentRoster::ATTENDED_NO,
        ProfessionalDevelopmentRoster::ATTENDED_YES,
    ];

    public function getProfileTitleAttribute()
    {
        if ( $this->user instanceof User && $this->user()->exists() ) {
            return $this->user->name;
        } elseif ( $this->wkuIdentity()->exists()) {
            return $this->wkuIdentity->title;
        } else {
            return "Anonymous";
        }
    }

    public function professionalDevelopmentSession()
    {
        return $this->belongsTo(ProfessionalDevelopmentSession::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wkuIdentity()
    {
        return $this->belongsTo(WkuIdentity::class);
    }

    public function attendanceForDepartment(Department $department, string $attendedValue = NULL)
    {
        $departmentId = $department->id;
        $query = ProfessionalDevelopmentRoster::query()
            ->select(['professional_development_rosters.*'])
            ->join('wku_identities as wi', function($join) use($departmentId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.department_id', '=', $departmentId);
            });

        if ( $attendedValue !== NULL ) {
            $query->where('professional_development_rosters.attended', '=', $attendedValue);
        }

        return $query;
    }

    public static function totalAttended()
    {
        return ProfessionalDevelopmentRoster::query()
        ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
    }

    public static function uniqueAttended()
    {
        return ProfessionalDevelopmentRoster::query()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->groupBy('professional_development_rosters.wku_identity_id');
    }

}
