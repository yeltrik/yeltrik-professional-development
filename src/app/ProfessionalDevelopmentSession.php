<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class ProfessionalDevelopmentSession
 * @property int id
 * @property int professional_development_program_id
 * @property int semester_id
 * @property string asana_gid
 * @property string title
 * @property string start_date_time
 * @property string end_date_time
 *
 * @property ProfessionalDevelopmentProgram professionalDevelopmentProgram
 * @property Semester semester
 * @package App
 */
class ProfessionalDevelopmentSession extends Model
{

    CONST ASANA_FACULTY_PD_PROJECT_GID = '1162954823800593';

    public function professionalDevelopmentProgram()
    {
        return $this->belongsTo(ProfessionalDevelopmentProgram::class);
    }

    public function professionalDevelopmentRosters()
    {
        return $this->hasMany(ProfessionalDevelopmentRoster::class);
    }

    public function semester()
    {
        return $this->belongsTo(Semester::class);
    }

    public function sessionsWithCountOfRosters()
    {
        return ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pdr_count'),
            ])
            ->leftJoin('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
            })
            ->groupBy('professional_development_sessions.id');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'professional_development_session_tag');
    }

}
