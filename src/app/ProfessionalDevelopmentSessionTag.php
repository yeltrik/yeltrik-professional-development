<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProfessionalDevelopmentSessionTag
 * @property int id
 * @property int professional_development_sessions_id
 * @property int tag_id
 *
 * @property ProfessionalDevelopmentSession professionalDevelopmentSession
 * @property Tag tag
 * @package App
 */
class ProfessionalDevelopmentSessionTag extends Model
{
    protected $table = 'professional_development_session_tag';

    public function professionalDevelopmentSession()
    {
        $this->belongsTo(ProfessionalDevelopmentSession::class);
    }

    public function tag()
    {
        $this->belongsTo(Tag::class);
    }

}
