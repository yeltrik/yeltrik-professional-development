<?php

namespace App\Http\Controllers;

use App\WkuIdentity;

class CollegeAttendanceController extends Abstract_AttendanceController
{
    CONST UNKNOWN_COLLEGE_NAME = '-- UnKnown --';

    protected function processWkuIdentity(array &$data, WkuIdentity $wkuIdentity)
    {
        if ($wkuIdentity instanceof WkuIdentity) {
            if ($wkuIdentity->college()->exists()) {
                $this->incrementData($data, $wkuIdentity->college->name);
            } else {
                $this->incrementData($data, $this->unknownKey());
            }
        }
    }

    public function unknownKey()
    {
        return static::UNKNOWN_COLLEGE_NAME;
    }

}
