<?php

namespace App\Http\Controllers;

use App\WkuIdentity;

class DepartmentAttendanceController extends Abstract_AttendanceController
{
    CONST UNKNOWN_DEPARTMENT_NAME = '-- UnKnown --';

    protected function processWkuIdentity(array &$data, WkuIdentity $wkuIdentity)
    {
        if ($wkuIdentity instanceof WkuIdentity) {
            if ($wkuIdentity->department()->exists()) {
                $this->incrementData($data, $wkuIdentity->department->name);
            } else {
                $this->incrementData($data, $this->unknownKey());
            }
        }
    }

    public function unknownKey()
    {
        return static::UNKNOWN_DEPARTMENT_NAME;
    }

}
