<?php

namespace App\Http\Controllers;

use App\College;
use App\PdCollege;
use App\ProfessionalDevelopmentRoster;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PdCollegeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', PdCollege::class);

        $colleges = PdCollege::colleges()
            ->paginate(10);

        $collegeParticipationData = (new PdCollegeRosterController())
            ->collegeParticipationData(ProfessionalDevelopmentRoster::uniqueAttended()->get());

        return view('pd.college.index', compact(
            'colleges',
            'collegeParticipationData'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param College $college
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(College $college)
    {
        $pdCollege = new PdCollege($college);
        $this->authorize('view', $pdCollege);

        $colleges = College::query()
            ->paginate(10);

        $totalParticipants = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->count();

        $uniqueParticipants = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->groupBy('wku_identity_id')
            ->get()
            ->count();

        return view('pd.college.show', compact(
            'college', 'colleges',
            'totalParticipants', 'uniqueParticipants'
        ));
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function tag(College $college)
    {
        $this->authorize('view', $college);

        return view('pd.college.tag', compact('college'));
    }

}
