<?php

namespace App\Http\Controllers;

use App\College;
use App\PdCollege;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdCollegeDepartmentController extends Controller
{

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $colleges = College::query()
            ->paginate(15);

        $pdCollege = new PdCollege($college);
        $departments = $pdCollege->departments()
            ->paginate(15);

        $departmentsWithUniqueParticipation = PdCollege::departmentsWithRosterAttendedUniqueCount($college);
//            ->distinct('wi.id')
//            ->get();
//            ->group
//            ->paginate(15);

        // For Chart
//        $collegeId = $college->id;
//        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
//            ->select(['professional_development_rosters.*'])
//            ->join('wku_identities as wi', function ($join) use ($collegeId) {
//                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
//                $join->where('wi.college_id', '=', $collegeId);
//            })
//            ->get();

//        $departmentAttendanceController = new DepartmentAttendanceController();
//        $departmentAttendanceData = $departmentAttendanceController
//            ->chartJsData($professionalDevelopmentRosters);

        return view('pd.college.department.index', compact(
            'college', 'colleges',
            'departments'
//            'departmentAttendanceData',
        ));
    }

}
