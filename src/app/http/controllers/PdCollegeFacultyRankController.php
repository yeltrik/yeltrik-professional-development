<?php

namespace App\Http\Controllers;

use App\College;
use App\Faculty;
use App\FacultyRank;
use App\ProfessionalDevelopmentRoster;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdCollegeFacultyRankController extends Controller
{

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $collegeId = $college->id;
        $uniqueAttendancesWithRank = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                'fr.id as faculty_rank_id',
                'fr.title as faculty_rank_title',
            ])
            ->join('wku_identities as wi', function ($join) use ($collegeId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', $collegeId);
                $join->join('faculties as f', function ($join) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->leftJoin('faculty_ranks as fr', function ($join) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                    });
                });
            })
            ->get();

        $uniqueAttendanceByFacultyRank = [];
        $stamp = [
            'faculty_rank' => NULL,
            'faculty_rank_id' => NULL,
            'faculty_rank_title' => NULL,
            'faculty_rank_count' => 0,
        ];
        foreach ($uniqueAttendancesWithRank as $uniqueAttendanceWithRank) {
            $facultyRankId = $uniqueAttendanceWithRank->faculty_rank_id;
            $facultyRankTitle = $uniqueAttendanceWithRank->faculty_rank_title;
            if ($facultyRankTitle != "") {
                $key = $facultyRankTitle;
            } else {
                $facultyRankTitle = "Faculty Rank Unknown for Attendee(s)";
                $key = $facultyRankTitle;
            }
            if (!array_key_exists($key, $uniqueAttendanceByFacultyRank)) {
                $uniqueAttendanceByFacultyRank[$key] = $stamp;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank'] = FacultyRank::query()->where('id', '=', $facultyRankId)->first();
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_id'] = $facultyRankId;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_title'] = $facultyRankTitle;
            }
            $uniqueAttendanceByFacultyRank[$key]['faculty_rank_count']++;
        }

        ksort($uniqueAttendanceByFacultyRank);

        return view('pd.college.faculty-rank.index', compact('college', 'uniqueAttendanceByFacultyRank'));
    }

    /**
     * @param College $college
     * @param FacultyRank $facultyRank
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function facultyRankUnique(College $college, FacultyRank $facultyRank)
    {
        $this->authorize('view', $college);
        $this->authorize('view', $facultyRank);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);
        $this->authorize('viewAny', Faculty::class);

        $collegeId = $college->id;
        $facultyRankId = $facultyRank->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                'fr.id as faculty_rank_id',
                'fr.title as faculty_rank_title',
            ])
            ->join('wku_identities as wi', function ($join) use ($collegeId, $facultyRankId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', $collegeId);
                $join->join('faculties as f', function ($join) use ($facultyRankId) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->join('faculty_ranks as fr', function ($join) use ($facultyRankId) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                        $join->on('fr.id', '=', $facultyRankId);
                    });
                });
            })
            ->paginate(10);

        return view('pd.college.faculty-rank.roster', compact('college', 'professionalDevelopmentRosters'));
    }

}
