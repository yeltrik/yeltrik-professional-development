<?php

namespace App\Http\Controllers;

use App\College;
use App\ProfessionalDevelopmentRoster;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\View\View;

/**
 * Class PdCollegeRosterController
 * @package App\Http\Controllers
 */
class PdCollegeRosterController extends Controller
{

    public function collegeParticipationData(Collection $professionalDevelopmentRosters): array
    {
        $collegeParticipationData = [];
        $stamp = [
            'college_id' => '',
            'college_name' => '',
            'attended_count' => 0,
        ];
        foreach ($professionalDevelopmentRosters as $roster) {
            if ($roster->wkuIdentity->college instanceof College) {
                $key = $roster->wkuIdentity->college->name;
                $id = $roster->wkuIdentity->college->id;
            } else {
                $key = "College Unknown for Attendee(s)";
                $id = NULL;
            }
            if (!array_key_exists($key, $collegeParticipationData)) {
                $collegeParticipationData[$key] = $stamp;
                $collegeParticipationData[$key]['college_name'] = $key;
                $collegeParticipationData[$key]['college_id'] = $id;
            }
            $collegeParticipationData[$key]['attended_count']++;
        }
        ksort($collegeParticipationData);

        return $collegeParticipationData;
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function total(College $college)
    {
        $this->authorize('view', $college);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $professionalDevelopmentRosters = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->paginate(10);

        return view('pd.college.roster.attended.total', compact('college', 'professionalDevelopmentRosters'));
    }

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function unique(College $college)
    {
        $this->authorize('view', $college);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $colleges = College::query()
            ->orderBy('name', 'asc')
            ->get();

        $collegeId = $college->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('wku_identities as wi', function ($join) use ($collegeId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', $collegeId);
            })
            ->paginate(10);

        return view('pd.college.roster.attended.unique', compact(
            'college', 'colleges',
            'professionalDevelopmentRosters'
        ));
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function uniqueUnknown()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $colleges = College::query()
            ->orderBy('name', 'asc')
            ->get();

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', null);
            })
            ->paginate(10);

        return view('pd.college.roster.attended.unique-unknown', compact(
            'professionalDevelopmentRosters',
            'colleges'
        ));
    }

}
