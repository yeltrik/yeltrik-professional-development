<?php

namespace App\Http\Controllers;

use App\College;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdCollegeSemesterController extends Controller
{

    /**
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(College $college)
    {
        $this->authorize('view', $college);

        $semesters = Semester::query()
            ->paginate(10);

        return view('pd.college.semester.index', compact('college', 'semesters'));
    }

    /**
     * @param College $college
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(College $college, Semester $semester)
    {
        $this->authorize('view', $college);

        return view('pd.college.semester.show', compact('college', 'semester'));
    }

}
