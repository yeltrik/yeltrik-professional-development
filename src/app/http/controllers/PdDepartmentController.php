<?php

namespace App\Http\Controllers;

use App\College;
use App\Department;
use App\ProfessionalDevelopmentProgram;
use App\ProfessionalDevelopmentRoster;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdDepartmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;
        $colleges = College::query()
            ->paginate(15);

        $departments = $college->departments();

        // TODO: Add those without Programs (or Uncategorized?)
        $professionalDevelopmentPrograms = (new ProfessionalDevelopmentProgram())
            ->programsForDepartmentWithAttendance($department, ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->paginate(10);

        return view('pd.department.show', compact(
            'department', 'departments',
            'college', 'colleges',
            'professionalDevelopmentPrograms'
        ));
    }

}
