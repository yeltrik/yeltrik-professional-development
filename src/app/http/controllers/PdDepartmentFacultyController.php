<?php

namespace App\Http\Controllers;

use App\Department;
use App\Faculty;
use Illuminate\Auth\Access\AuthorizationException;

class PdDepartmentFacultyController extends Controller
{

    /**
     * @param Department $department
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);
        $this->authorize('viewAny', Faculty::class);
    }

}
