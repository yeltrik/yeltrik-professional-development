<?php

namespace App\Http\Controllers;

use App\Department;
use App\FacultyRank;
use Illuminate\Auth\Access\AuthorizationException;

class PdDepartmentFacultyRankController extends Controller
{

    /**
     * @param Department $department
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);
        $this->authorize('viewAny', FacultyRank::class);
    }

    /**
     * @param Department $department
     * @param FacultyRank $facultyRank
     * @throws AuthorizationException
     */
    public function show(Department $department, FacultyRank $facultyRank)
    {
        $this->authorize('view', $department);
        $this->authorize('view', $facultyRank);
    }

}
