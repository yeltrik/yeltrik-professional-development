<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdDepartmentProgramController extends Controller
{

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        return view('pd.department.program', compact('department', 'college'));
    }

}
