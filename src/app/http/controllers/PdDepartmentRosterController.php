<?php

namespace App\Http\Controllers;

use App\Department;
use App\ProfessionalDevelopmentRoster;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdDepartmentRosterController extends Controller
{

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        $attendedValue = NULL;
        $professionalDevelopmentRosters = (new ProfessionalDevelopmentRoster())
            ->attendanceForDepartment($department, $attendedValue)
            ->paginate(10);

        $professionalDevelopmentRostersGet = (new ProfessionalDevelopmentRoster())
            ->attendanceForDepartment($department, $attendedValue)
            ->get();

        $rosterSummaryUnique = (new PdParticipationController())
            ->rosterSummaryUnique($professionalDevelopmentRostersGet, PdParticipationController::SORT_COUNT_DESC);

        $totalParticipationCount = (new PdParticipationController())
            ->totalParticipationCount($professionalDevelopmentRostersGet);
        $uniqueParticipationCount = (new PdParticipationController())
            ->uniqueParticipationCount($professionalDevelopmentRostersGet);

        return view('pd.department.roster.index', compact(
            'department', 'college',
            'uniqueParticipationCount', 'totalParticipationCount',
            'professionalDevelopmentRosters',
            'rosterSummaryUnique'
        ));
    }

}
