<?php

namespace App\Http\Controllers;

use App\Department;
use App\ProfessionalDevelopmentSession;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdDepartmentSessionController extends Controller
{

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        $departmentId = $department->id;
        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pd_roster_count')
            ])
            ->join('professional_development_rosters as pdr', function($join) use($departmentId){
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                $join->join('wku_identities as wi', function($join) use($departmentId){
                    $join->on('wi.id', '=', 'pdr.wku_identity_id');
                    $join->where('wi.department_id', '=', $departmentId);
                });
            })
            ->orderBy('pd_roster_count', 'desc')
            //->orderBy('professional_development_sessions.title', 'asc')
            ->groupBy('professional_development_sessions.id')
            ->paginate(10);

        return view('pd.department.session.index', compact(
            'department', 'college',
            'professionalDevelopmentSessions'
        ));
    }

}
