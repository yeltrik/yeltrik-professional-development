<?php

namespace App\Http\Controllers;

use App\Department;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdDepartmentSessionTagController extends Controller
{

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $college = $department->college;

        $tags = static::tags($department)
            ->paginate(10);

        return view('pd.department.session.tag.index', compact(
            'college',
            'department',
            'tags'
        ));
    }

    public function show(Department $department, Tag $tag)
    {
        $this->authorize('view', $department);
        $this->authorize('viewAny', $tag);

        $college = $department->college;

        $tags = static::tags($department)
            ->paginate(10);

        return view('pd.department.session.tag.show', compact(
            'college',
            'department',
            'tag', 'tags'
        ));
    }

    /**
     * @param Department $department
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function tags(Department $department)
    {
        $departmentId = $department->id;
        return Tag::query()
            ->select([
                'tags.*',
                DB::raw('count(pdr.id) as pd_roster_count'),
            ])
            ->join('professional_development_session_tag as pdst', function ($join) use ($departmentId) {
                $join->on('pdst.tag_id', '=', 'tags.id');
                $join->join('professional_development_sessions as pds', function ($join) use ($departmentId) {
                    $join->on('pds.id', '=', 'pdst.professional_development_session_id');
                    $join->join('professional_development_rosters as pdr', function ($join) use ($departmentId) {
                        $join->on('pdr.professional_development_session_id', '=', 'pds.id');
                        $join->join('wku_identities as wi', function ($join) use ($departmentId) {
                            $join->on('wi.id', '=', 'pdr.wku_identity_id');
                            $join->where('wi.department_id', '=', $departmentId);
                        });
                    });
                });
            })
            ->orderBy('pd_roster_count', 'desc')
            ->groupBy('tags.id');
    }

}
