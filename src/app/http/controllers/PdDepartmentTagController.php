<?php

namespace App\Http\Controllers;

use App\Department;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdDepartmentTagController extends Controller
{

    /**
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Department $department)
    {
        $this->authorize('view', $department);

        $tags = Tag::query()
            ->orderBy('name', 'asc')
            ->paginate(15);

        $college = $department->college;

        return view('pd.department.tag', compact(
            'department', 'college',
            'tags'
        ));
    }

}
