<?php

namespace App\Http\Controllers;

use App\FacultyRank;
use App\FacultyRankTopX;
use App\PdFacultyRank;
use App\PD;
use App\ProfessionalDevelopmentRoster;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PdFacultyRankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', PD::class);
        $this->authorize('viewAny', FacultyRank::class);

        $uniqueAttendancesWithRank = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                'fr.id as faculty_rank_id',
                'fr.title as faculty_rank_title',
            ])
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->join('faculties as f', function ($join) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->leftJoin('faculty_ranks as fr', function ($join) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                    });
                });
            })
            ->get();

        $uniqueAttendanceByFacultyRank = [];
        $stamp = [
            'faculty_rank_id' => NULL,
            'faculty_rank_title' => NULL,
            'faculty_rank_count' => 0,
        ];
        foreach ($uniqueAttendancesWithRank as $uniqueAttendanceWithRank) {
            $facultyRankId = $uniqueAttendanceWithRank->faculty_rank_id;
            $facultyRankTitle = $uniqueAttendanceWithRank->faculty_rank_title;
            if ($facultyRankTitle != "") {
                $key = $facultyRankTitle;
            } else {
                $facultyRankTitle = "Faculty Rank Unknown for Attendee(s)";
                $key = $facultyRankTitle;
            }
            if (!array_key_exists($key, $uniqueAttendanceByFacultyRank)) {
                $uniqueAttendanceByFacultyRank[$key] = $stamp;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_id'] = $facultyRankId;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_title'] = $facultyRankTitle;
            }
            $uniqueAttendanceByFacultyRank[$key]['faculty_rank_count']++;
        }

        ksort($uniqueAttendanceByFacultyRank);

        $allData = [];
//        dd($uniqueAttendanceByFacultyRank);
        foreach ($uniqueAttendanceByFacultyRank as $key => $array) {
            $allData[$key] = $array['faculty_rank_count'] ;
        }

        $facultyRankTopX = new FacultyRankTopX();
        $facultyRankTopXData = $facultyRankTopX->getTopXData($allData, true, 5, 'Other Ranks');

        return view('pd.faculty-rank.index', compact(
            'uniqueAttendanceByFacultyRank',
            'facultyRankTopXData'
        ));

    }

    /**
     * Display the specified resource.
     *
     * @param FacultyRank $facultyRank
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(FacultyRank $facultyRank)
    {
        $this->authorize('viewAny', PD::class);
        $this->authorize('view', $facultyRank);

        return view('pd.faculty-rank.show', compact('facultyRank'));
    }

    /**
     * @param FacultyRank $facultyRank
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function unique(FacultyRank $facultyRank)
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);
        $this->authorize('view', $facultyRank);

        $facultyRankId = $facultyRank->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('wku_identities as wi', function ($join) use($facultyRankId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->join('faculties as f', function ($join) use($facultyRankId) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->join('faculty_ranks as fr', function ($join) use($facultyRankId) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                        $join->where('fr.id', '=', $facultyRankId);
                    });
                });
            })
            ->paginate(10);
        return view('pd.faculty-rank.table.unique-roster', compact('professionalDevelopmentRosters'));
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function uniqueUnknown()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', FacultyRank::class);

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select(['professional_development_rosters.*'])
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->join('faculties as f', function ($join) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->where('f.faculty_rank_id', '=', null);
                });
            })
            ->paginate(10);
        return view('pd.faculty-rank.table.unique-roster', compact('professionalDevelopmentRosters'));
    }

}
