<?php

namespace App\Http\Controllers;

class PdParticipationController extends Controller
{

    const SORT_COUNT_DESC = 'sortCountDesc';

    const SESSION_COUNT_KEY = 'sessionCount';
    const WKU_IDENTITY_KEY = 'wkuIdentity';

    public function rosterSummaryUnique($professionalDevelopmentRosters, $sort = NULL)
    {
        $rosterSummaryUnique = [];
        $stamp = [
            static::WKU_IDENTITY_KEY => null,
            static::SESSION_COUNT_KEY => 0,
        ];

        foreach ($professionalDevelopmentRosters as $item) {
            $key = $item->wku_identity_id;
            if (!array_key_exists($key, $rosterSummaryUnique)) {
                $rosterSummaryUnique[$key] = $stamp;
                $rosterSummaryUnique[$key][static::WKU_IDENTITY_KEY] = $item->wkuIdentity;
            }
            $rosterSummaryUnique[$key][static::SESSION_COUNT_KEY]++;
        }

        switch ($sort) {
            case null :
                break;
            case (static::SORT_COUNT_DESC) :
                usort($rosterSummaryUnique, ['static', 'usortSessionCountDesc']);
                break;
        }

        return $rosterSummaryUnique;
    }

    public function totalParticipationCount($professionalDevelopmentRosters)
    {
        return $professionalDevelopmentRosters->count();
    }

    public function uniqueParticipationCount($professionalDevelopmentRosters)
    {
        $participants = [];
        foreach ($professionalDevelopmentRosters as $item) {
            $key = $item->wku_identity_id;
            if (!array_key_exists($key, $participants)) {
                $participants[$key] = true;
            }
        }
        return sizeof($participants);
    }

    public static function usortSessionCountDesc($a, $b)
    {
        if ($a[static::SESSION_COUNT_KEY] == $b[static::SESSION_COUNT_KEY]) {
            return 0;
        }
        return ($a[static::SESSION_COUNT_KEY] < $b[static::SESSION_COUNT_KEY]) ? 1 : -1;
    }

}
