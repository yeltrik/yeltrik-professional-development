<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentProgram;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PdProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentProgram::class);

        $professionalDevelopmentPrograms = ProfessionalDevelopmentProgram::query()
            ->orderBy('title')
            ->paginate(50);

        return view('pd.program.index', compact('professionalDevelopmentPrograms'));
    }

    /**
     * Display the specified resource.
     *
     * @param ProfessionalDevelopmentProgram $professionalDevelopmentProgram
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(ProfessionalDevelopmentProgram $professionalDevelopmentProgram)
    {
        $this->authorize('view', $professionalDevelopmentProgram);

        $professionalDevelopmentSessions = $professionalDevelopmentProgram
            ->professionalDevelopmentSessions()
            ->orderBy('start_date_time', 'desc')
            ->paginate(10);

        return view('pd.program.show',
            compact('professionalDevelopmentProgram', 'professionalDevelopmentSessions'));
    }

}
