<?php

namespace App\Http\Controllers;

use App\Department;
use App\PdProgramDepartment;
use App\ProfessionalDevelopmentProgram;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PdProgramDepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return void
     */
    public function index()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param ProfessionalDevelopmentProgram $professionalDevelopmentProgram
     * @param Department $department
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(ProfessionalDevelopmentProgram $professionalDevelopmentProgram, Department $department)
    {
        $pdProgramDepartment = new PdProgramDepartment();
        $pdProgramDepartment->professionalDevelopmentProgram = $professionalDevelopmentProgram;
        $pdProgramDepartment->department = $department;
        $this->authorize('view', $pdProgramDepartment);

        $departmentId = $department->id;
        $pdProgramDepartment = new PdProgramDepartment();
        $pdProgramDepartment->professionalDevelopmentProgram = $professionalDevelopmentProgram;
        $professionalDevelopmentRosters = $pdProgramDepartment->attendanceForDepartment($department, NULL)
            ->paginate(10);

        return view('pd.program.department.show', compact(
            'professionalDevelopmentProgram', 'department',
            'professionalDevelopmentRosters'
        ));
    }

}
