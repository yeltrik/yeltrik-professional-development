<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdRosterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->paginate(15);

        $professionalDevelopmentSessions = (new ProfessionalDevelopmentSession())
            ->sessionsWithCountOfRosters()
            ->get();

        $with = 'With Roster Data';
        $without = 'Without Roster Data';
        $rosterData = [$with => 0, $without => 0];
        $attendanceData = [ProfessionalDevelopmentRoster::ATTENDED_YES => 0, ProfessionalDevelopmentRoster::ATTENDED_NO => 0];
        foreach ($professionalDevelopmentSessions as $professionalDevelopmentSession) {
            if (
                $professionalDevelopmentSession->pdr_count > 0
            ) {
                $rosterData[$with]++;
            } else {
                $rosterData[$without]++;
            }
        }

        $attendanceData = [
            ProfessionalDevelopmentRoster::ATTENDED_YES => ProfessionalDevelopmentRoster::query()
                ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
                ->count(),
            ProfessionalDevelopmentRoster::ATTENDED_NO => ProfessionalDevelopmentRoster::query()
                ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_NO)
                ->count()
        ];

        $totalParticipants = ProfessionalDevelopmentRoster::totalAttended()
            ->get()
            ->count();

        $uniqueParticipants = ProfessionalDevelopmentRoster::uniqueAttended()
            ->get()
            ->count();


        return view('pd.roster.index', compact(
            'professionalDevelopmentRosters',
            'rosterData',
            'attendanceData',
            'totalParticipants',
            'uniqueParticipants'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param ProfessionalDevelopmentRoster $professionalDevelopmentRoster
     * @return void
     */
    public function show(ProfessionalDevelopmentRoster $professionalDevelopmentRoster)
    {
        //
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function total()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::totalAttended()
            ->select(['professional_development_rosters.*'])
            ->join('wku_identities as wi', function($join){
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
            })
            ->orderBy('wi.name', 'asc')
            ->orderBy('wi.email', 'asc')
            ->paginate(10);

        return view('pd.roster.total', compact('professionalDevelopmentRosters'));

    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function unique()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                DB::raw('count(professional_development_rosters.professional_development_session_id) as sessions_attended_count')
                ])
            ->join('wku_identities as wi', function($join){
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
            })
            ->orderBy('wi.name', 'asc')
            ->orderBy('wi.email', 'asc')
            ->groupBy(['wi.id'])
            ->paginate(10);

        return view('pd.roster.unique', compact('professionalDevelopmentRosters'));
    }

}
