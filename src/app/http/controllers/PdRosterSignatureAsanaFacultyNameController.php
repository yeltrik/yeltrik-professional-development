<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\WkuIdentity;

class PdRosterSignatureAsanaFacultyNameController extends Abstract_PdRosterSignatureController
{

    const ASANA_FACULTY_NAME_KEY = 'Asana Faculty Name';
    const ATTENDANCE_NAME_KEY = 'Attendance Name';
    const REGISTRATION_NAME_KEY = 'Registration Name';

    private array $signatureFacultyName = [
        'Asana Faculty Name',
        'Attendance Name',
        'Registration Name',
    ];

    public function getSignature(): array
    {
        $signature = $this->signatureFacultyName;
        asort($signature);
        return $signature;
    }

    public function getWkuIdentity(array $rosterRecord)
    {
        $wkuIdentity = NULL;

        $registrationName = $rosterRecord[static::REGISTRATION_NAME_KEY];
        $attendanceName = $rosterRecord[static::ATTENDANCE_NAME_KEY];
        $asanaFacultyName = $rosterRecord[static::ASANA_FACULTY_NAME_KEY];

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $registrationName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $registrationName)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $attendanceName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $attendanceName)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $asanaFacultyName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $asanaFacultyName)
                ->first();
        }

        if ($asanaFacultyName != NULL) {
            $name = $asanaFacultyName;
        } elseif ($registrationName != NULL) {
            $name = $registrationName;
        } elseif ($attendanceName != NULL) {
            $name = $attendanceName;
        } else {
            $name = NULL;
        }

        if ($wkuIdentity == NULL && static::CREATE_WKU_IDENTITY) {
            $wkuIdentity = new WkuIdentity();
            $wkuIdentity->name = $name;
            $wkuIdentity->save();
//            dd([
//                $registrationName,
//                $attendanceName,
//                $asanaFacultyName,
//                $wkuIdentity,
//            ]);
        }


        return $wkuIdentity;
    }

    public function process(ProfessionalDevelopmentSession $professionalDevelopmentSession, array $rosterArray)
    {
        foreach ($rosterArray as $rosterRecord) {
            if ($this->rosterRowMatches($rosterRecord)) {
                $wkuIdentity = $this->getWkuIdentity($rosterRecord);

                $attendanceName = $rosterRecord[static::ATTENDANCE_NAME_KEY];

                $user = NULL;

                if ($attendanceName != NULL) {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_YES;
                } else {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_NO;
                }

                if ($wkuIdentity instanceof WkuIdentity) {
                    $pdRoster = $this->getPdRoster($professionalDevelopmentSession, $user, $wkuIdentity, $attendedValue);
                } else {
                    dd([
                        'Unable to Process Roster Record',
                        $rosterRecord,
                    ]);
                }

//                dd([
//                    $wkuIdentity,
//                    $rosterRecord
//                ]);

            }
        }
    }

}
