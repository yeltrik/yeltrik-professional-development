<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\User;
use App\WkuIdentity;
use Asana\Client;

class PdRosterSignatureRegistrationAttendanceController extends Abstract_PdRosterSignatureController
{

    const REGISTRATION_DESIGNATION_KEY = 'Registration Designation';
    const ATTENDANCE_EMAIL_KEY = 'Attendance Email';
    const ATTENDANCE_NAME_KEY = 'Attendance Name';
    const REGISTRATION_NAME_KEY = 'Registration Name';
    const REGISTRATION_EMAIL_KEY = 'Registration Email';
    const ASANA_FACULTY_NAME_KEY = 'Asana Faculty Name';

    private array $signatureRegistrationAttendance = [
        0 => 'Registration Email',
        1 => 'Registration Name',
        2 => 'Registration Designation',
        3 => 'Registration Staff Department',
        4 => 'Attendance Name',
        5 => 'Attendance Designation',
        6 => 'Attendance Department',
        7 => 'Attendance Email',
        8 => 'Asana Faculty Name',
    ];

    protected function getPdRoster(ProfessionalDevelopmentSession $professionalDevelopmentSession, User $user = NULL, WkuIdentity $wkuIdentity, string $attended)
    {
        $pdRoster = ProfessionalDevelopmentRoster::query()
            ->where('wku_identity_id', '=', $wkuIdentity->id)
            ->where('professional_development_session_id', '=', $professionalDevelopmentSession->id)
            ->first();
        if ($pdRoster === NULL) {
            $pdRoster = $this->newProfessionalDevelopmentRoster($professionalDevelopmentSession, $user, $wkuIdentity, $attended, ProfessionalDevelopmentRoster::STUDENT_ROLE);
        }
        return $pdRoster;
    }

    public function getSignature(): array
    {
        $signature = $this->signatureRegistrationAttendance;
        asort($signature);
        return $signature;
    }

    public function getUser(array $rosterRecord)
    {
        // Check for Email, if not then check for WKU identity, that could have an email

        $registrationEmail = $rosterRecord[static::REGISTRATION_EMAIL_KEY];
        $attendanceEmail = $rosterRecord[static::ATTENDANCE_EMAIL_KEY];

        if (
            $registrationEmail !== NULL &&
            $attendanceEmail !== NULL &&
            $registrationEmail === $attendanceEmail
        ) {
            $email = $registrationEmail;
            //dd($email);
            $user = User::query()
                ->where('email', '=', $email)
                ->get()
                ->first();
            if ($user instanceof User) {
                return $user;
            }

        }

        return NULL;
    }

    public function getWkuIdentity(array $rosterRecord)
    {
        $wkuIdentity = NULL;

        //dd($rosterRecord);

        $registrationEmail = $rosterRecord[static::REGISTRATION_EMAIL_KEY];
        $attendanceEmail = $rosterRecord[static::ATTENDANCE_EMAIL_KEY];
        $registrationName = $rosterRecord[static::REGISTRATION_NAME_KEY];
        $attendanceName = $rosterRecord[static::ATTENDANCE_NAME_KEY];
        $asanaFacultyName = $rosterRecord[static::ASANA_FACULTY_NAME_KEY];

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $registrationEmail != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('email', '=', $registrationEmail)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $attendanceEmail != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('email', '=', $attendanceEmail)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $registrationName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $registrationName)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $attendanceName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $attendanceName)
                ->first();
        }

        if ($wkuIdentity instanceof WkuIdentity === FALSE && $asanaFacultyName != NULL) {
            $wkuIdentity = WkuIdentity::query()
                ->where('name', '=', $asanaFacultyName)
                ->first();
        }

        $email = NULL;
        if ($registrationEmail !== NULL) {
            $email = $registrationEmail;
        } else {
            $email = $attendanceEmail;
        }

        if ($asanaFacultyName != NULL) {
            $name = $asanaFacultyName;
        } elseif ($registrationName != NULL) {
            $name = $registrationName;
        } elseif ($attendanceName != NULL) {
            $name = $attendanceName;
        } else {
            $name = $email;
        }

        if ($wkuIdentity == NULL && static::CREATE_WKU_IDENTITY) {
            $wkuIdentity = new WkuIdentity();
            $wkuIdentity->name = $name;
            if ( $email != NULL ) {
                $wkuIdentity->email = $email;
            }
            $wkuIdentity->save();
//            dd([
//                $registrationEmail,
//                $attendanceEmail,
//                $registrationName,
//                $attendanceName,
//                $asanaFacultyName,
//                $wkuIdentity,
//            ]);
        }

        if ($wkuIdentity instanceof WkuIdentity && static::UPDATE_ASANA === TRUE && $wkuIdentity->isAsana()) {
            $this->updateAsana($wkuIdentity, $rosterRecord, $email);
        }

        return $wkuIdentity;
    }

    public function process(ProfessionalDevelopmentSession $professionalDevelopmentSession, array $rosterArray)
    {
        foreach ($rosterArray as $rosterRecord) {
            if ($this->rosterRowMatches($rosterRecord)) {

                $wkuIdentity = $this->getWkuIdentity($rosterRecord);

                $user = NULL;

                $attendanceName = $rosterRecord[static::ATTENDANCE_NAME_KEY];
                $attendanceEmail = $rosterRecord[static::ATTENDANCE_EMAIL_KEY];

                if (
                    $attendanceName != NULL ||
                    $attendanceEmail != NULL
                ) {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_YES;
                } else {
                    $attendedValue = ProfessionalDevelopmentRoster::ATTENDED_NO;
                }

                if ($wkuIdentity instanceof WkuIdentity) {
                    $pdRoster = $this->getPdRoster($professionalDevelopmentSession, $user, $wkuIdentity, $attendedValue);
                } else {
                    dd([
                        'Unable to Process Roster Record',
                        $rosterRecord,
                    ]);
                }
                //dd([$user, $wkuIdentity, $pdRoster]);
            }
        }

        //dd([$this->signatureRegistrationAttendance, $array]);
        return redirect()->route('pds.sessions.show', $professionalDevelopmentSession);

    }

    public function updateAsana(WkuIdentity $wkuIdentity, array $rosterRecord, string $email)
    {
        $updateArray = [];

        // Look for things that are Null
        if ($wkuIdentity->email == NULL && $email != NULL) {
            // Fetch Asana Record
            $asana_client = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $task = $asana_client->tasks->getTask($wkuIdentity->asana_gid);

            //dd($task);

            // Compare Asana Task / Custom Field value
            foreach ($task->custom_fields as $customField) {
//                    dd($customField);
                if ($customField->name === 'Email' && $email != "") {
                    //dd($customField->name);
                    if (!array_key_exists('custom_fields', $updateArray)) {
                        $updateArray['custom_fields'] = [];
                    }
                    $updateArray['custom_fields'][$customField->gid] = $email;

//                        dd($updateArray);

                    // TODO: Need to Update Task
                    $result = $asana_client
                        ->tasks
                        ->updateTask($task->gid,
                            $updateArray,
                            array('opt_pretty' => 'true')
                        );

                    // Update the Database
                    $wkuIdentity->email = $email;
                    $wkuIdentity->save();

                    echo "Updating: <pre> " . var_export(
                            [
                                $rosterRecord,
                                $updateArray,
//                            $result,
//                            $wkuIdentity,
                            ]
                            , TRUE) . "</pre>";
                }
            }
        }
    }

}
