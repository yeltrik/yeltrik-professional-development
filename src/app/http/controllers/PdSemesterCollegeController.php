<?php

namespace App\Http\Controllers;

use App\College;
use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdSemesterCollegeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function colleges(Semester $semester)
    {
        $semesterId = $semester->id;
        return College::query()
            ->select(['colleges.*'])
            ->join('wku_identities as wi', function ($join) use ($semesterId) {
                $join->on('wi.college_id', '=', 'colleges.id');
                $join->join('professional_development_rosters as pdr', function ($join) use ($semesterId) {
                    $join->on('pdr.wku_identity_id', '=', 'wi.id');
                    $join->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                        $join->on('pds.id', '=', 'pdr.professional_development_session_id');
                        $join->where('pds.semester_id', '=', $semesterId);
                    });
                });
            })
            ->orderBy('colleges.name', 'asc')
            ->groupBy('colleges.id');
    }

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('view', $semester);

        $semesters = PdSemesterController::semesters()
            ->paginate(10);

        $colleges = static::colleges($semester)
            ->paginate(15);

        $semesterId = $semester->id;
        $collegeParticipationData = (new PdCollegeRosterController())
            ->collegeParticipationData(
                ProfessionalDevelopmentRoster::uniqueAttended()
                    ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                        $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                        $join->where('pds.semester_id', '=', $semesterId);
                    })
                    ->get()
            );

//        $semesterId = $semester->id;
//        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
//            ->where('professional_development_rosters.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
//            ->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
//                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
//                $join->where('pds.semester_id', '=', $semesterId);
//            });

//        $collegeAttendanceController = new CollegeAttendanceController();
//        $collegeAttendanceData = $collegeAttendanceController
//            ->chartJsData($professionalDevelopmentRosters->get());

        return view('pd.semester.college.index', compact(
            'semester', 'semesters',
            'colleges',
//            'collegeAttendanceData',
            'collegeParticipationData'
        ));
    }

    /**
     * @param Semester $semester
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Semester $semester, College $college)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $college);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $colleges = static::colleges($semester)
            ->paginate(10);

        // With Roster Data
        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pdr_count'),
            ])
            ->where('professional_development_sessions.semester_id', '=', $semester->id)
            ->leftJoin('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
            })
            ->groupBy('professional_development_sessions.id');

        $with = 'With Roster Data';
        $without = 'Without Roster Data';
        $rosterData = [$with => 0, $without => 0];
        foreach ($professionalDevelopmentSessions->get() as $professionalDevelopmentSession) {
            if (
                $professionalDevelopmentSession->pdr_count > 0
            ) {
                $rosterData[$with]++;
            } else {
                $rosterData[$without]++;
            }
        }

        $semesterId = $semester->id;
        $collegeId = $college->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->where('professional_development_rosters.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->join('wku_identities as wi', function ($join) use ($collegeId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', $collegeId);
            })
            ->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            });

        $departmentAttendanceController = new DepartmentAttendanceController();
        $departmentAttendanceData = $departmentAttendanceController
            ->chartJsData($professionalDevelopmentRosters->get());

        $semesterId = $semester->id;
        $totalParticipants = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->join('professional_development_sessions as pds', function($join) use($semesterId){
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->count();

        $uniqueParticipants = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->join('professional_development_sessions as pds', function($join) use($semesterId){
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->groupBy('wku_identity_id')
            ->get()
            ->count();

        return view('pd.semester.college.show', compact(
            'semester', 'semesters',
            'college','colleges',
            'departmentAttendanceData',
            'totalParticipants', 'uniqueParticipants'
        ));

    }

}
