<?php

namespace App\Http\Controllers;

use App\College;
use App\PdSemester;
use App\PdSemesterCollege;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterCollegeDepartmentController extends Controller
{

    /**
     * @param Semester $semester
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester, College $college)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $college);

        //$pdSemester = new PdSemester($semester);
        $semesters = PdSemester::semesters()
            ->paginate('10');

        $pdSemesterCollege = new PdSemesterCollege($semester, $college);
        $colleges = $pdSemesterCollege->colleges()
            ->paginate(15);

        $departments = $pdSemesterCollege->departments()
            ->paginate(15);

//        dd($departments->get());

        return view('pd.semester.college.department.index', compact(
            'semester', 'semesters',
            'college', 'colleges',
            'departments'
        ));
    }

}
