<?php

namespace App\Http\Controllers;

use App\College;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterCollegeFacultyRankController extends Controller
{

    /**
     * @param Semester $semester
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester, College $college)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $college);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $colleges = PdSemesterCollegeController::colleges($semester)
            ->paginate(10);

        return view('pd.semester.college.faculty-rank.index', compact(
            'semester', 'college',
            'semesters', 'colleges'
        ));
    }

}
