<?php

namespace App\Http\Controllers;

use App\College;
use App\ProfessionalDevelopmentRoster;
use App\Semester;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterCollegeRosterController extends Controller
{

    /**
     * @param Semester $semester
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function total(Semester $semester, College $college)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $college);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $semesterId = $semester->id;
        $professionalDevelopmentRosters = $college->professionalDevelopmentRosters()
            ->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->paginate(10);

        return view('pd.college.roster.attended.total', compact('college', 'professionalDevelopmentRosters'));
    }

    /**
     * @param Semester $semester
     * @param College $college
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function unique(Semester $semester, College $college)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $college);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $semesterId = $semester->id;
        $collegeId = $college->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('wku_identities as wi', function ($join) use ($collegeId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', $collegeId);
            })
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->paginate(10);

        return view('pd.college.roster.attended.unique', compact('college', 'professionalDevelopmentRosters'));
    }

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function uniqueUnknown(Semester $semester)
    {
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $semesterId = $semester->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->where('wi.college_id', '=', null);
            })
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->paginate(10);

        return view('pd.college.roster.attended.unique-unknown', compact('professionalDevelopmentRosters'));
    }

}
