<?php

namespace App\Http\Controllers;

use App\PdSemester;
use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdSemesterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', PdSemester::class);

        $semesters = static::semesters()
            ->paginate('10');

        return view('pd.semester.index', compact('semesters'));
    }

    public static function semesters()
    {
        return Semester::query()
            ->select([
                'semesters.*',
                DB::raw('count(pds.id) as professional_development_session_count')
            ])
            ->join('professional_development_sessions as pds', function($join){
                $join->on('pds.semester_id', '=', 'semesters.id');
            })
            ->groupBy('semesters.id');
    }

    /**
     * Display the specified resource.
     *
     * @param Semester $semester
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Semester $semester)
    {
        $pdSemester = new PdSemester($semester);

        $this->authorize('view', $pdSemester);

        $semesters = static::semesters()
            ->paginate('10');

        // With Roster Data
        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pdr_count'),
            ])
            ->where('professional_development_sessions.semester_id', '=', $semester->id)
            ->leftJoin('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
            })
            ->groupBy('professional_development_sessions.id');

        $with = 'With Roster Data';
        $without = 'Without Roster Data';
        $rosterData = [$with => 0, $without => 0];
        foreach($professionalDevelopmentSessions->get() as $professionalDevelopmentSession) {
            if (
                $professionalDevelopmentSession->pdr_count > 0
            ) {
                $rosterData[$with]++;
            } else {
                $rosterData[$without]++;
            }
        }

        $semesterId = $semester->id;
        $attendanceData = [
            ProfessionalDevelopmentRoster::ATTENDED_YES => ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
                })
                ->count(),
            ProfessionalDevelopmentRoster::ATTENDED_NO =>  ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_NO);
                })
                ->count(),
        ];

        return view('pd.semester.show', compact(
            'semester', 'semesters', 'rosterData', 'attendanceData'
        ));
    }

}
