<?php

namespace App\Http\Controllers;

use App\Department;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterDepartmentController extends Controller
{

    /**
     * @param Semester $semester
     * @param Department $department
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Semester $semester, Department $department)
    {
        $this->authorize('view', $department);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $college = $department->college;

        $colleges = PdSemesterCollegeController::colleges($semester)
            ->paginate(10);

        $departments = $college->departments;

        return view('pd.semester.department.show', compact(
            'semester', 'semesters',
            'college', 'colleges',
            'department', 'departments'
        ));
    }

}
