<?php

namespace App\Http\Controllers;

use App\Faculty;
use App\FacultyRank;
use App\ProfessionalDevelopmentRoster;
use App\Semester;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterFacultyRankController extends Controller
{

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('view', $semester);
        $this->authorize('viewAny', FacultyRank::class);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $facultyRanks = FacultyRank::query()
            ->paginate(10);

        $semesterId = $semester->id;
        $uniqueAttendancesWithRank = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                'fr.id as faculty_rank_id',
                'fr.title as faculty_rank_title',
            ])
            ->join('wku_identities as wi', function ($join){
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->join('faculties as f', function ($join) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->leftJoin('faculty_ranks as fr', function ($join) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                    });
                });
            })
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->get();

        $uniqueAttendanceByFacultyRank = [];
        $stamp = [
            'faculty_rank' => NULL,
            'faculty_rank_id' => NULL,
            'faculty_rank_title' => NULL,
            'faculty_rank_count' => 0,
        ];
        foreach ($uniqueAttendancesWithRank as $uniqueAttendanceWithRank) {
            $facultyRankId = $uniqueAttendanceWithRank->faculty_rank_id;
            $facultyRankTitle = $uniqueAttendanceWithRank->faculty_rank_title;
            if ($facultyRankTitle != "") {
                $key = $facultyRankTitle;
            } else {
                $facultyRankTitle = "Faculty Rank Unknown for Attendee(s)";
                $key = $facultyRankTitle;
            }
            if (!array_key_exists($key, $uniqueAttendanceByFacultyRank)) {
                $uniqueAttendanceByFacultyRank[$key] = $stamp;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank'] = FacultyRank::query()->where('id', '=', $facultyRankId)->first();
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_id'] = $facultyRankId;
                $uniqueAttendanceByFacultyRank[$key]['faculty_rank_title'] = $facultyRankTitle;
            }
            $uniqueAttendanceByFacultyRank[$key]['faculty_rank_count']++;
        }

        ksort($uniqueAttendanceByFacultyRank);

        return view('pd.semester.faculty-rank.index', compact(
            'semester', 'semesters',
            'facultyRanks', 'uniqueAttendanceByFacultyRank'));
    }

    /**
     * @param Semester $semester
     * @param FacultyRank $facultyRank
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(Semester $semester, FacultyRank $facultyRank)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $facultyRank);

        return view('pd.semester.faculty-rank.show', compact('semester', 'facultyRank'));
    }

    /**
     * @param Semester $semester
     * @param FacultyRank $facultyRank
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function facultyRankUnique(Semester $semester, FacultyRank $facultyRank)
    {
        $this->authorize('view', $semester);
        $this->authorize('view', $facultyRank);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);
        $this->authorize('viewAny', Faculty::class);

        $semesterId = $semester->id;
        $facultyRankId = $facultyRank->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                'fr.id as faculty_rank_id',
                'fr.title as faculty_rank_title',
            ])
            ->join('wku_identities as wi', function ($join) use ($facultyRankId) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
                $join->join('faculties as f', function ($join) use ($facultyRankId) {
                    $join->on('f.wku_identity_id', '=', 'wi.id');
                    $join->join('faculty_ranks as fr', function ($join) use ($facultyRankId) {
                        $join->on('fr.id', '=', 'f.faculty_rank_id');
                        $join->on('fr.id', '=', $facultyRankId);
                    });
                });
            })
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->paginate(10);

        return view('pd.semester.faculty-rank.roster', compact('semester', 'facultyRank', 'professionalDevelopmentRosters'));
    }

}
