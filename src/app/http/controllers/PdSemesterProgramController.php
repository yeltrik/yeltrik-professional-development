<?php

namespace App\Http\Controllers;

use App\PdSemesterProgram;
use App\ProfessionalDevelopmentProgram;
use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdSemesterProgramController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Semester $semester
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('viewAny', PdSemesterProgram::class);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $semesterId = $semester->id;
        $professionalDevelopmentPrograms = ProfessionalDevelopmentProgram::query()
            ->select(['professional_development_programs.*'])
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.professional_development_program_id', '=', 'professional_development_programs.id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->groupBy('professional_development_programs.title')
            ->orderBy('title')
            ->paginate(10);

        // With Roster Data
        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pdr_count'),
            ])
            ->where('professional_development_sessions.semester_id', '=', $semester->id)
            ->leftJoin('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
            })
            ->groupBy('professional_development_sessions.id')
            ->get();

        $with = 'With Roster Data';
        $without = 'Without Roster Data';
        $rosterData = [$with => 0, $without => 0];
        foreach($professionalDevelopmentSessions as $professionalDevelopmentSession) {
            if (
                $professionalDevelopmentSession->pdr_count > 0
            ) {
                $rosterData[$with]++;
            } else {
                $rosterData[$without]++;
            }
        }

        $semesterId = $semester->id;
        $attendanceData = [
            ProfessionalDevelopmentRoster::ATTENDED_YES => ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
                })
                ->count(),
            ProfessionalDevelopmentRoster::ATTENDED_NO =>  ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_NO);
                })
                ->count(),
        ];

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->where('professional_development_rosters.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->leftJoin('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            });

        $collegeAttendanceController = new CollegeAttendanceController();
        $collegeAttendanceData = $collegeAttendanceController
            ->chartJsData($professionalDevelopmentRosters->get());

        $departmentAttendanceController = new DepartmentAttendanceController();
        $departmentAttendanceData = $departmentAttendanceController
            ->chartJsData($professionalDevelopmentRosters->get());

        return view('pd.semester.program.index', compact(
            'semester', 'semesters',
            'professionalDevelopmentPrograms',
            'rosterData', 'attendanceData',
            'collegeAttendanceData', 'departmentAttendanceData'
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param Semester $semester
     * @param ProfessionalDevelopmentProgram $professionalDevelopmentProgram
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Semester $semester, ProfessionalDevelopmentProgram $professionalDevelopmentProgram)
    {
        $pdSemesterProgram = new PdSemesterProgram();
        $pdSemesterProgram->semester = $semester;
        $pdSemesterProgram->professionalDevelopmentProgram = $professionalDevelopmentProgram;
        $this->authorize('view', $pdSemesterProgram);

        // With Roster Data
        $professionalDevelopmentSessionsQuery = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as pdr_count'),
            ])
            ->where('professional_development_sessions.semester_id', '=', $semester->id)
            ->where('professional_development_sessions.professional_development_program_id', '=', $professionalDevelopmentProgram->id)
            ->leftJoin('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
            })
            ->groupBy('professional_development_sessions.id');

        // With Roster Data
        $with = 'With Roster Data';
        $without = 'Without Roster Data';
        $rosterData = [$with => 0, $without => 0];
        foreach($professionalDevelopmentSessionsQuery->get() as $professionalDevelopmentSession) {
            if (
                $professionalDevelopmentSession->pdr_count > 0
            ) {
                $rosterData[$with]++;
            } else {
                $rosterData[$without]++;
            }
        }

        $semesterId = $semester->id;
        $attendanceData = [
            ProfessionalDevelopmentRoster::ATTENDED_YES => ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->where('professional_development_sessions.professional_development_program_id', '=', $professionalDevelopmentProgram->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
                })
                ->count(),
            ProfessionalDevelopmentRoster::ATTENDED_NO =>  ProfessionalDevelopmentSession::query()
                ->where('professional_development_sessions.semester_id', '=', $semester->id)
                ->where('professional_development_sessions.professional_development_program_id', '=', $professionalDevelopmentProgram->id)
                ->join('professional_development_rosters as pdr', function($join) {
                    $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                    $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_NO);
                })
                ->count(),
        ];

        $pdProgramId = $professionalDevelopmentProgram->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->select([
                'professional_development_rosters.*',
            ])
            ->where('professional_development_rosters.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)
            ->join('professional_development_sessions as pds', function($join) use($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->join('professional_development_programs as pdp', function($join) use($pdProgramId) {
                $join->on('pdp.id', '=', 'pds.professional_development_program_id');
                $join->where('pdp.id', '=', $pdProgramId);
            });

        $collegeAttendanceController = new CollegeAttendanceController();
        $collegeAttendanceData = $collegeAttendanceController
            ->chartJsData($professionalDevelopmentRosters->get());

        $departmentAttendanceController = new DepartmentAttendanceController();
        $departmentAttendanceData = $departmentAttendanceController
            ->chartJsData($professionalDevelopmentRosters->get());

        $professionalDevelopmentSessions = $professionalDevelopmentSessionsQuery->paginate(10);

        return view('pd.semester.program.show', compact(
            'semester', 'professionalDevelopmentProgram', 'professionalDevelopmentSessions',
            'rosterData', 'attendanceData',
            'collegeAttendanceData', 'departmentAttendanceData'
        ));
    }

}
