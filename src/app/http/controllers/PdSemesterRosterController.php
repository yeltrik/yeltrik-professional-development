<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentRoster;
use App\Semester;
use App\WkuIdentity;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class PdSemesterRosterController extends Controller
{

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('view', $semester);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::query()
            ->paginate(10);

        $totalParticipants = PdSemesterRosterController::totalAttended($semester);
        $uniqueParticipants = PdSemesterRosterController::uniqueAttended($semester);

        return view('pd.semester.roster.index', compact(
            'semester', 'semesters',
            'professionalDevelopmentRosters',
            'totalParticipants',
            'uniqueParticipants'
        ));
    }

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function total(Semester $semester)
    {
        $this->authorize('view', $semester);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $semesterId = $semester->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::totalAttended()
            ->select(['professional_development_rosters.*'])
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
            })
            ->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->orderBy('wi.name', 'asc')
            ->orderBy('wi.email', 'asc')
            ->paginate(10);

        return view('pd.semester.roster.total', compact('semester', 'professionalDevelopmentRosters'));

    }

    /**
     * @param Semester $semester
     * @return int
     */
    public static function totalAttended(Semester $semester)
    {
        $semesterId = $semester->id;
        return ProfessionalDevelopmentRoster::totalAttended()
            ->join('professional_development_sessions as pds', function($join) use($semesterId){
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('semester_id', '=', $semesterId);
            })
            ->get()
            ->count();
    }

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function unique(Semester $semester)
    {
        $this->authorize('view', $semester);
        $this->authorize('viewAny', ProfessionalDevelopmentRoster::class);
        $this->authorize('viewAny', WkuIdentity::class);

        $semesterId = $semester->id;
        $professionalDevelopmentRosters = ProfessionalDevelopmentRoster::uniqueAttended()
            ->select([
                'professional_development_rosters.*',
                DB::raw('count(professional_development_rosters.professional_development_session_id) as sessions_attended_count')
            ])
            ->join('wku_identities as wi', function ($join) {
                $join->on('wi.id', '=', 'professional_development_rosters.wku_identity_id');
            })
            ->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->orderBy('wi.name', 'asc')
            ->orderBy('wi.email', 'asc')
            ->groupBy(['wi.id'])
            ->paginate(10);

        return view('pd.semester.roster.unique', compact('semester', 'professionalDevelopmentRosters'));
    }

    /**
     * @param Semester $semester
     * @return int
     */
    public static function uniqueAttended(Semester $semester)
    {
        $semesterId = $semester->id;
        return ProfessionalDevelopmentRoster::uniqueAttended()
            ->join('professional_development_sessions as pds', function ($join) use ($semesterId) {
                $join->on('pds.id', '=', 'professional_development_rosters.professional_development_session_id');
                $join->where('pds.semester_id', '=', $semesterId);
            })
            ->get()
            ->count();
    }

}
