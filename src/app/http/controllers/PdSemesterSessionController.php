<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentSession;
use App\Semester;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterSessionController extends Controller
{

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('view', $semester);
        $this->authorize('viewAny', ProfessionalDevelopmentSession::class);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->paginate(15);

        return view('pd.semester.session.index', compact('semester', 'semesters',
            'professionalDevelopmentSessions'));
    }

}
