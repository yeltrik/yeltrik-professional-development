<?php

namespace App\Http\Controllers;

use App\Semester;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PdSemesterTagController extends Controller
{

    /**
     * @param Semester $semester
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function index(Semester $semester)
    {
        $this->authorize('view', $semester);

        $semesters = PdSemesterController::semesters()
            ->paginate('10');

        $tags = Tag::query()
            ->paginate(10);

        return view('pd.semester.tag.index', compact(
            'semester', 'semesters',
            'tags'
        ));
    }

}
