<?php

namespace App\Http\Controllers;

use App\AsanaExportSeeding_Trait;
use App\AsanaTaskCustomField_Trait;
use App\ProfessionalDevelopmentProgram;
use App\ProfessionalDevelopmentRoster;
use App\ProfessionalDevelopmentSession;
use App\Semester;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use ProfessionalDevelopmentProgramSeeder;

class PdSessionController extends Controller
{

    use AsanaExportSeeding_Trait;
    use AsanaTaskCustomField_Trait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', ProfessionalDevelopmentSession::class);

        $professionalDevelopmentSessions = ProfessionalDevelopmentSession::query()
            ->select([
                'professional_development_sessions.*',
                DB::raw('count(pdr.id) as roster_attended_count'),
            ])
            ->join('professional_development_rosters as pdr', function ($join) {
                $join->on('pdr.professional_development_session_id', '=', 'professional_development_sessions.id');
                $join->where('pdr.attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES);
            })
            ->groupBy('professional_development_sessions.id')
            ->orderBy('roster_attended_count', 'desc')
            ->paginate(10);

        return view('pd.session.index', compact('professionalDevelopmentSessions'));
    }

    /**
     * Display the specified resource.
     *
     * @param ProfessionalDevelopmentSession $professionalDevelopmentSession
     * @return Application|Factory|RedirectResponse|Response|View
     * @throws AuthorizationException
     */
    public function show(ProfessionalDevelopmentSession $professionalDevelopmentSession)
    {
        $this->authorize('view', $professionalDevelopmentSession);

        if ($professionalDevelopmentSession->professionalDevelopmentRosters()->count() === 0) {
            return redirect()->route('pds.sessions.asana-task-scan-attachments', $professionalDevelopmentSession);
        }

        $professionalDevelopmentRosters = $professionalDevelopmentSession->professionalDevelopmentRosters()
            ->paginate(10);

        $professionalDevelopmentSessionId = $professionalDevelopmentSession->id;
        $uniqueParticipants = ProfessionalDevelopmentRoster::uniqueAttended()
            ->where('professional_development_session_id', '=', $professionalDevelopmentSessionId)
            ->get()
            ->count();

        $roster = $professionalDevelopmentSession->professionalDevelopmentRosters()->count();
        $attended = $professionalDevelopmentSession->professionalDevelopmentRosters()->where('attended', '=', ProfessionalDevelopmentRoster::ATTENDED_YES)->count();

        $attendanceData = [
            "Attended" => $attended,
            "Not Attended" => $roster - $attended
        ];

//        $collegeAttendanceController = new CollegeAttendanceController();
//        $collegeAttendanceData = $collegeAttendanceController
//            ->chartJsData($professionalDevelopmentSession->professionalDevelopmentRosters);
//
//        $departmentAttendanceController = new DepartmentAttendanceController();
//        $departmentAttendanceData = $departmentAttendanceController
//            ->chartJsData($professionalDevelopmentSession->professionalDevelopmentRosters);

        return view('pd.session.show', compact(
            'professionalDevelopmentSession', 'professionalDevelopmentRosters',
            'attendanceData',
            'uniqueParticipants'
//                'collegeAttendanceData', 'departmentAttendanceData'
        ));
    }

    public function importAsanaTask(array $task)
    {
        $professionalDevelopmentPrograms = ProfessionalDevelopmentProgram::all();
        $professionalDevelopmentProgramUncategorized = ProfessionalDevelopmentProgram::query()
            ->where('title', '=', ProfessionalDevelopmentProgramSeeder::UNCATEGORIZED_TITLE)
            ->firstOrFail();

        $name = $task['name'];
        $dueOn = $task['due_on'];
        $gid = $task['gid'];
        $tags = $task['tags'];

        $customFields = $task['custom_fields'];

        $semesterName = static::getCustomFieldEnumValue($customFields, 'Semester');
        if ($semesterName !== NULL) {
            $semester = $this->getSemester($semesterName);
        } else {
            $semester = NUll;
        }

        if ($name == NULL) {
            echo "Null Name for PD Task: " . $task['gid'];
        } else {
            $professionalDevelopmentSession = new ProfessionalDevelopmentSession();

            // Attempt to Find Matching
            $found = FALSE;
            foreach ($professionalDevelopmentPrograms as $professionalDevelopmentProgram) {
                $title = $professionalDevelopmentProgram->title;
                if (stristr($name, $title)) {
                    $found = $professionalDevelopmentProgram;
                    continue;
                }
            }
            if ($found === FALSE) {
                $professionalDevelopmentSession->professionalDevelopmentProgram()->associate($professionalDevelopmentProgramUncategorized);
            } else {
                //dd([$name, $title, $found]);
                $professionalDevelopmentSession->professionalDevelopmentProgram()->associate($found);
            }

            if ($semester instanceof Semester) {
                $professionalDevelopmentSession->semester()->associate($semester);
            }

            $professionalDevelopmentSession->asana_gid = $gid;
            $professionalDevelopmentSession->title = $name;
            $professionalDevelopmentSession->start_date_time = $dueOn;
            $professionalDevelopmentSession->save();

            if (sizeof($tags) > 0) {
                foreach ($tags as $tagArray) {
                    $tagArray = (array)$tagArray;
                    $tag = Tag::query()->where('asana_gid', '=', $tagArray['gid'])->first();
                    if ($tag instanceof Tag === FALSE) {
                        $tag = new Tag();
                        $tag->asana_gid = $tagArray['gid'];
                        $tag->name = $tagArray['name'];
                        if (array_key_exists('color', $tagArray)) {
                            $tag->color = $tagArray['color'];
                        }
                        $tag->save();
                    }
                    $professionalDevelopmentSession->tags()->attach($tag);
                }
            }
        }
    }

}
