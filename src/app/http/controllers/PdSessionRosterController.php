<?php

namespace App\Http\Controllers;

use App\ProfessionalDevelopmentSession;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class PdSessionRosterController extends Controller
{

    /**
     * @param ProfessionalDevelopmentSession $professionalDevelopmentSession
     * @throws AuthorizationException
     */
    public function index(ProfessionalDevelopmentSession $professionalDevelopmentSession)
    {
        $this->authorize('view', $professionalDevelopmentSession);

        return view('pd.session.roster.index', compact(
            'professionalDevelopmentSession',
        ));
    }

}
