<?php

namespace App\Http\Controllers;

use App\PdTag;
use App\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;

class PdTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', PdTag::class);

        $tags = Tag::query()
//            ->select([
//                'tags.*',
//                DB::raw('count(pdst.id) as professional_development_session_count'),
//            ])
//            ->join('professional_development_session_tag as pdst', function($join){
//                $join->on('pdst.tag_id', '=', 'tags.id');
//            })
//            ->groupBy('tags.id')
            ->orderBy('name', 'asc')
//            ->orderBy('professional_development_session_count', 'desc')
        ->get();

        return view('pd.tag.index', compact('tags'));
    }

    /**
     * Display the specified resource.
     *
     * @param Tag $tag
     * @return Application|Factory|Response|View
     * @throws AuthorizationException
     */
    public function show(Tag $tag)
    {
        $pdTag = new PdTag();
        $pdTag->tag = $tag;
        $this->authorize('view', $pdTag);

        $professionalDevelopmentSessions = $tag->professionalDevelopmentSessions()
            ->paginate(10);

        return view('pd.tag.show', compact('tag', 'professionalDevelopmentSessions'));
    }

}
