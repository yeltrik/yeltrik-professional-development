<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalDevelopmentSessionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_development_sessions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('professional_development_program_id');
            $table->unsignedBigInteger('semester_id')->nullable();
            $table->string('asana_gid')->nullable();
            $table->text('title');
            $table->dateTime('start_date_time')->nullable();
            $table->dateTime('end_date_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_development_sessions');
    }
}
