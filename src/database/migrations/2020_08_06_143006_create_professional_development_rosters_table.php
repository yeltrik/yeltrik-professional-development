<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalDevelopmentRostersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_development_rosters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('professional_development_session_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->unsignedBigInteger('wku_identity_id')->nullable();
            $table->enum('role', \App\ProfessionalDevelopmentRoster::$roles);
            $table->enum('attended', \App\ProfessionalDevelopmentRoster::$attendedOptions);
            //$table->enum('present', \App\ProfessionalDevelopmentRoster::$presentOptions);
            //$table->enum('enrolled', \App\ProfessionalDevelopmentRoster::$enrolledOptions);
            //$table->enum('waiting_list', \App\ProfessionalDevelopmentRoster::$waitingListOptions);
            //$table->enum('revoked', \App\ProfessionalDevelopmentRoster::$revokedOptions);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_development_rosters');
    }
}
