<?php

use Illuminate\Database\Seeder;

class ProfessionalDevelopmentProgramSeeder extends Seeder
{

    CONST UNCATEGORIZED_TITLE = '-- Uncategorized --';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->new(static::UNCATEGORIZED_TITLE);

        $this->new('Lecturing With Zoom');
        $this->new('Teaching When Being in the classroom is not possible');
        $this->new('Recording Lectures Using Mediasite');
        $this->new('BB Basics');
        $this->new('5 Charges to Create More Inclusive (online) Classrooms');
        $this->new('Planning Your Hybrid Course');
        $this->new('Basics of Hybrid Teaching');
        $this->new('Exploring Blackboard Test Options to Reduce Cheating');
        $this->new('How to Create an Engaging Zoom Class Meeting');
        $this->new('How to Use Zoom');
        $this->new('Math Department Meeting');
        $this->new('Student Presentations – How to Transition Student Face-to-Face Presentations to the Online Environment');
    }

    private function new(string $title)
    {
        $pdp = new \App\ProfessionalDevelopmentProgram();
        $pdp->title = $title;
        $pdp->save();
        return $pdp;
    }
}
