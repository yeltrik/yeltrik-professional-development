<?php

use Illuminate\Database\Seeder;

class ProfessionalDevelopmentRosterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $professionalDevelopmentSessions = \App\ProfessionalDevelopmentSession::all();

        foreach($professionalDevelopmentSessions as $professionalDevelopmentSession) {
            $teacherCount = rand(0,2);
            for($i=0; $i<=$teacherCount; $i++) {
                $this->new($professionalDevelopmentSession, \App\ProfessionalDevelopmentRoster::TEACHER_ROLE);
            }

            $studentCount = rand(0,80);
            for($i=0; $i<=$studentCount; $i++) {
                $this->new($professionalDevelopmentSession, \App\ProfessionalDevelopmentRoster::STUDENT_ROLE);
            }
        }
    }

    public function new(\App\ProfessionalDevelopmentSession $professionalDevelopmentSession, string $role)
    {
        $professionalDevelopmentRoster = new \App\ProfessionalDevelopmentRoster();
        $professionalDevelopmentRoster->professionalDevelopmentSession()->associate($professionalDevelopmentSession);
        $professionalDevelopmentRoster->role = $role;

        if ( rand(0,3) === 0 ) {
            $user = \App\User::all()->random(1)[0];
            $professionalDevelopmentRoster->user()->associate($user);
            $professionalDevelopmentRoster->wkuIdentity()->associate($user->wkuIdentity);
        }

        $professionalDevelopmentRoster->save();
        return $professionalDevelopmentRoster;
    }
}
