<?php

use App\Http\Controllers\PdSessionController;
use Illuminate\Database\Seeder;

class ProfessionalDevelopmentSessionSeeder extends Seeder
{

    use \App\AsanaExportSeeding_Trait;
    use \App\AsanaTaskCustomField_Trait;

    // Custom Fields
    //Category
    //College
    //Department
    //Registered
    //Attended
    //Faculty Attended
    //Staff Attended
    //Fac Registered
    //Staff Registered
    //Speaker
    //Food
    //Semester

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->seedRandom();

        $json = json_decode(Storage::disk('public')->get('faculty_pd.json'), true);

        $professionalDevelopmentSesionController = new PdSessionController();

        foreach ($json as $data) {
            foreach ($data as $key => $task) {
                $professionalDevelopmentSesionController->importAsanaTask($task);
            }
        }
    }

    private function new(\App\ProfessionalDevelopmentProgram $professionalDevelopmentProgram)
    {
        $professionalDevelopmentSession = new \App\ProfessionalDevelopmentSession();
        $professionalDevelopmentSession->professionalDevelopmentProgram()->associate($professionalDevelopmentProgram);

        $professionalDevelopmentSession->title = $professionalDevelopmentProgram->title;

        $randomize = rand(-145, 15);
        $randomtime = time() + $randomize * 86400;
        $date = date('Y-m-d', $randomtime);
        $professionalDevelopmentSession->start_date_time = $date;

        $professionalDevelopmentSession->save();
        return $professionalDevelopmentSession;
    }

    private function seedRandom()
    {
        $professionalDevelopmentPrograms = \App\ProfessionalDevelopmentProgram::all();

        foreach ($professionalDevelopmentPrograms as $professionalDevelopmentProgram) {
            $sessionCount = rand(0, 10);
            for ($i = 0; $i <= $sessionCount; $i++) {
                $this->new($professionalDevelopmentProgram);
            }
        }
    }

}
